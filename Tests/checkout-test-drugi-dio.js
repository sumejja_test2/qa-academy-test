const productDetails = require('../pages/product-details-page.js');
const shoppingCart = require('../pages/frontend/shopping-cart-page');
const page = require('../pages/checkout-page');
const shippingCheckout = require('../pages/shipping-checkout-page.js');
const login = require('../pages/login-page');
const orderDetails = require('../pages/order-details-page');
const D = require('../data-provider/test-data');
const helper = require('../e2e-helper');

fdescribe('Testing using page-object model', function () {

    beforeAll(function () {
        const currentDate = helper.getCurrentDateAndTime('dddd, mmmm dd, yyyy');
        console.log('Current date is ............' + helper.currentDate);
            });

    beforeEach(function () {
        browser.driver.manage().deleteAllCookies();
        browser.get('https://demo.nopcommerce.com/adidas-consortium-campus-80s-running-shoes');
        });

    it('Verify that Shipping address can be set as different than the Billing Address', function () {

        productDetails.select_shoes_size('10')
            .select_shoes_color()
            .click_ADD_TO_CART_button()
            .click_Shopping_cart_link_on_notification();
        shoppingCart.click_terms_of_service_checkbox()
            .click_CHECKOUT_button();
        login.click_CHECKOUT_AS_GUEST_button();
        page.click_checkbox_ship_to_same_address();
        page.enter_all_required_values_billing_address(D.orderDetails)
            .click_CONTINUE_on_Billing_Address();
        page.click_checkbox_new_shipping_address();
        page.select_new_shipping_address_dropdown(`New Address`);
        page.enter_all_required_values_shipping_address(D.orderDetailsShippingAddress)
            .click_CONTINUE_on_Shipping_Address();
        page.click_CONTINUE_on_Shipping_Method();
        page.click_CONTINUE_on_Payment_Method();
        page.click_CONTINUE_on_Payment_Information();
        page.click_CONTINUE_ORDER();
        page.click_ORDER_DETAILS_LINK();

        orderDetails.verify_Products_values(D.product);
        orderDetails.verify_different_address();
    });

    it ('Verify that Shipping Method can be set to Air-NextDay', function () {

        productDetails.select_shoes_size('10')
            .select_shoes_color()
            .click_ADD_TO_CART_button()
            .click_Shopping_cart_link_on_notification();
        shoppingCart.click_terms_of_service_checkbox()
            .click_CHECKOUT_button();
        login.click_CHECKOUT_AS_GUEST_button();
        page.click_checkbox_ship_to_same_address();
        page.enter_all_required_values_billing_address(D.orderDetails)
            .click_CONTINUE_on_Billing_Address();
        page.click_checkbox_new_shipping_address();
        page.select_new_shipping_address_dropdown(`New Address`);
        page.enter_all_required_values_shipping_address(D.orderDetailsShippingAddress)
            .click_CONTINUE_on_Shipping_Address();
        page.choose_shipping_method();
        page.click_CONTINUE_on_Shipping_Method();
        page.click_CONTINUE_on_Payment_Method();
        page.click_CONTINUE_on_Payment_Information();
        page.click_CONTINUE_ORDER();
        page.click_ORDER_DETAILS_LINK();

        orderDetails.verify_shipping_method(D.shippingMethod.nextDayAir);
    });

    it('Verify that Payment Method can be set to Credit Card',function () {

        productDetails.select_shoes_size('10')
            .select_shoes_color()
            .click_ADD_TO_CART_button()
            .click_Shopping_cart_link_on_notification();
        shoppingCart.click_terms_of_service_checkbox()
            .click_CHECKOUT_button();
        login.click_CHECKOUT_AS_GUEST_button();
        page.click_checkbox_ship_to_same_address();
        page.enter_all_required_values_billing_address(D.orderDetails)
            .click_CONTINUE_on_Billing_Address();
        page.click_checkbox_new_shipping_address();
        page.select_new_shipping_address_dropdown(`New Address`);
        page.enter_all_required_values_shipping_address(D.orderDetailsShippingAddress)
            .click_CONTINUE_on_Shipping_Address();
        page.choose_shipping_method();
        page.click_CONTINUE_on_Shipping_Method();
        page.choose_payment_method();
        page.click_CONTINUE_on_Payment_Method();
        page.enter_payment_information(D.paymentInfo);
        page.click_CONTINUE_on_Payment_Information();
        page.click_CONTINUE_ORDER();
        page.click_ORDER_DETAILS_LINK();

        orderDetails.verify_payment_method(D.paymentMethod.creditCard);
    });
});
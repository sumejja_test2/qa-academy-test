const admin = require('../pages/admin/add-product-page');
const D = require('../data-provider/test-data');

describe('Testing adding product', function () {

    beforeEach(function () {
        browser.driver.manage().deleteAllCookies();
        admin.enter_Admin_Panel()
            .enter_Product_Page()

    });

    it('Verify that new product can be saved to Electronics and Cell Phones category', function () {

        admin.add_new_product(D.administratorAddProductData)
            .verify_new_product_added();
    });

    it('Verify that new product is published after adding to Electronics and Cell Phones category', function () {

        admin.add_new_product(D.administratorAddProductData)
            .verify_new_product_published();
    });

    it('Verify that new product can be saved and published with image', function () {

        admin.add_new_product(D.administratorAddProductData, true)
            .click_Save_and_Continue_Edit()
            .upload_image('Picture1.jpg')

        browser.sleep(5000)
    });
});
const helper = require('../e2e-helper.js');
const page = require('../pages/my-account-page');
const registration = require('../pages/registration-page');
const menu = require('../pages/menu');
const D = require('../data-provider/test-data');
const C = require('../data-provider/constants');

describe('My Account Page', function () {

    beforeEach(function () {

        D.registrationDetails.emailReg = helper.getRandomString(5) + '@yopmail.com';

        browser.driver.manage().deleteAllCookies();
        browser.get(C.baseUrl);
        menu.click_Register();
        registration.enter_all_required_values(D.registrationDetails);
        registration.click_Register_button();
        registration.click_Continue_button();
        menu.click_MyAccount();
    });

    fit('Verify that all values are displayed on My Account page upon successful registration', function () {

        page.verify_all_values(D.registrationDetails);
    });

    it('Verify that Customer Info from Registration Page can be changed on My Account page', function () {

        page.change_personal_info(D.registrationDetails_edited);
        page.verify_all_values(D.registrationDetails_edited);
        return this;
    });

    it('Verify that Password from Registration Page can be changed on My Account page', function () {

        page.change_password_on_account_page();
        page.verify_password_change_message(C.pages.myAccountDetails.messageChangedPassword);
    });
});
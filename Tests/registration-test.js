const page = require('../pages/registration-page');
const menu = require('../pages/menu');
const D = require('../data-provider/test-data');
const C = require('../data-provider/constants');

fdescribe('Registration Page', function () {

    beforeEach(function () {
        browser.driver.manage().deleteAllCookies();
        browser.get(C.baseUrl);
        menu.click_Register();
    });

    afterEach(function () {
        D.registrationValues = {
            firstName: D.randomNo + '_auto-test_FirstName',
            lastName: D.randomNo + '_auto-test_LastName',
            email: D.getNewRandomNo() + '_auto-test@yopmail.com',
            password: 'Test123',
            confirmPassword: 'Test123',
        };
    });

    it('Verify successful user registration', function () {
        page.enter_all_required_values(D.registrationValues);
        page.click_Register_button();
        page.verify_successful_registration();
    });

    it('Verify validation messages for required fields', function () {

        page.click_Register_button();
        page.verify_validation_messages(C.pages.registration.validationMessages.requiredFields);
    });

    it('Verify validation message for existing email', function () {
        D.registrationValues.email = D.accounts.admin.email;

        page.enter_all_required_values(D.registrationValues)
            .click_Register_button()
            .verify_validation_message(C.pages.registration.validationMessages.existingEmail);
    });

    it('Verify validation message for wrong email', function () {

        D.registrationValues.email = 'wrongEmailFormat@';
        page.enter_all_required_values(D.registrationValues)
            .click_Register_button()
            .verify_validation_message(C.pages.registration.validationMessages.wrongEmail);
    });

    it('Verify validation message for too short password', function () {

        D.registrationValues.password = 'short';
        D.registrationValues.confirmPassword = 'short';
        page.enter_all_required_values(D.registrationValues)
            .click_Register_button()
            .verify_validation_message(C.pages.registration.validationMessages.passwordTooShort);
    });

    it('Verify validation message for passwords that do not match', function () {

        D.registrationValues.password = 'Test123456'
        page.enter_all_required_values(D.registrationValues)
            .click_Register_button()
            .verify_validation_message(C.pages.registration.validationMessages.passwordsDoNotMatch);
    });
});
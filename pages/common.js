const helper = require('../e2e-helper.js');
const addToCartButton = $('.add-to-cart-button');

const CommonElementsAndFunctions = function () {
    
    this.click_ = function () {
        helper.waitAndClick(addToCartButton)
        return this;
    };
};

module.exports = new CommonElementsAndFunctions();

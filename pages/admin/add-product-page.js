const helper = require('../../e2e-helper.js');
const D = require('../../data-provider/test-data');
const C = require('../../data-provider/constants');

const loginLink = $('.ico-login');
const emailInputField = $('.email')
const passwordInputField = $('.password');
const loginButton = $('.login-button');
const header = $('.header-upper');
const administratorLink = $('[href="/Admin"]');
const catalogSubmenu = $('.fa-book');
const product = $('[href="/Admin/Product/List"]');
const addNewProductButton = $('[href="/Admin/Product/Create"]');
const productName = $('#Name');
const shortDescription = $('#ShortDescription');
const fullDescription = $('#tinymce');
const iFrameFullDesc = element(by.tagName('iframe'));
const SKU = $('#Sku');
const categoriesDropdown = $('.k-widget.k-multiselect.k-header');
const price = $('#Price');
const taxCategory = $('#TaxCategoryId');
const inventoryMethod = $('#ManageInventoryMethodId');
const stockQuantity = $('#StockQuantity');
const saveButton =  $('[name="save"]');
const saveAndContinueEditButton = $('[name="save-continue"]');
const successMessage = $('.alert.alert-success.alert-dismissable');
const searchProductName = $('#SearchProductName');
const searchButton = $('#search-products');
const publishedField = $('#SearchPublishedId');
const firstProductOnTheList = $('#products-grid_wrapper');

const AddProductPage = function () {

    this.login_As_Admin = function (Data) {
        browser.get(C.currentUrl);
        helper.click(loginLink);
        helper.enterValue(emailInputField, Data.emailAdmin);
        helper.enterValue(passwordInputField, Data.passwordAdmin);
        helper.click(loginButton);
        return this;
    };

    this.enter_Admin_Panel = function () {
        this.login_As_Admin(D.administratorLoginData);
        helper.click(administratorLink);
        return this;
    };

    this.enter_Product_Page = function () {
        helper.click(catalogSubmenu);
        helper.click(product);
        return this;
    };

    this.click_Save_and_Continue_Edit = function () {
        helper.click(saveAndContinueEditButton)
        return this;
    };

    this.upload_image = function (filename) {

        helper.uploadFile(filename);
        return this;
    };

    this.add_new_product = function (data) {
        helper.click(addNewProductButton);
        helper.enterValue(productName, data.productName);
        helper.enterValue(shortDescription, data.shortDescription);

        // browser.switchTo().frame(iFrameFullDesc);
        //fullDescription.sendKeys(data.fullDescription);
        helper.enterValue(SKU, data.SKU);
        helper.selectItemnOnDropdownList(categoriesDropdown, data.category);

        helper.enterValue(price, 500);
        // helper.selectDropdownOption(taxCategory, D.taxCategoryDropdown.value2);
        // helper.selectDropdownOption(inventoryMethod, D.inventoryMethodDropdown.value1);
        // helper.enterValue(stockQuantity, data.stockQuantity);
        return this;
    };

    this.verify_new_product_added = function () {
        helper.verifyText(successMessage, C.pages.administration.addProduct.successMessage);
        return this;
    };

    this.verify_new_product_published = function () {
        helper.enterValue(searchProductName, D.administratorAddProductData.productName);
        helper.selectDropdownOption(publishedField, 'Published only');
        helper.click(searchButton);

        helper.waitTextToAppearOnElement(firstProductOnTheList, D.administratorAddProductData.productName)
        helper.verifyText(firstProductOnTheList, D.administratorAddProductData.productName);
        return this;
    };
};

module.exports = new AddProductPage;
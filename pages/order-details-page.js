const helper = require('../e2e-helper.js');
const orderDate = $('.order-date');
const orderNumber = $('.order-number');
const orderStatus = $('.order-status');
const billingInfo = $('.billing-info-wrap');
const shippingInfo = $('.shipping-info-wrap');
const nameBillingAddress = $$('.name').get(0);
const nameShippingAddress = $$('.name').get(1);
const emailBillingAddress = $('.billing-info-wrap').$('.email');
const emailShippingAddress = $('.shipping-info-wrap').$('.email');
const phoneBillingAddress = $('.billing-info-wrap').$('.phone');
const phoneShippingAddress = $$('.phone').get(1);
const address1BillingAddress = $$('.address1').get(0);
const address1ShippingAddress = $$('.address1').get(1);
const cityStateZipBillingAddress = $$('.city-state-zip').get(0);
const cityStateZipShippingAddress = $$('.city-state-zip').get(1);
const countryBillingAddress = $$('.country').get(0);
const countryShippingAddress = $$('.country').get(1);
const paymentMethod = $('.payment-method');
const paymentMethodStatus = $('.payment-method-status');
const shippingMethod = $('.shipping-method');
const shippingStatus = $('.shipping-status');
const skuProduct = $('.sku-number');
const productName = element.all(by.tagName('tbody')).get(0).$('.product');
const attributesSizeColor = $('.attributes');
const productUnitPrice = $('.product-unit-price');
const productQuantity = $('.product-quantity');
const productSubTotal = $('.product-subtotal');
const totalInfoSection = element.all(by.tagName('tbody')).get(1);
const subtotal = totalInfoSection.all(by.tagName('tr')).get(0);
const shipping = totalInfoSection.all(by.tagName('tr')).get(1);
const tax = totalInfoSection.all(by.tagName('tr')).get(2);
const orderTotal = totalInfoSection.all(by.tagName('tr')).get(3);
const addressBillingFinal = $$('.address1').get(0);
const addressShippingFinal = $$('.address1').get(1);
const paymentMethodFinal = $$('.payment-method span').get(1);
const shippingMethodFinal = $$('.shipping-method span').get(1);



const OrderDetailsPage = function () {

    this.verify_Order_date = function (label) {
        helper.verifyText(orderDate, label);
        return this;
    };
    this.verify_Order_number = function (msg) {
        helper.verifyText(orderNumber, msg);
        return this;
    };

    this.verify_Billing_Address_values = function (orderDetails, pageStaticValues, paymentMethodValue, paymentStatusValue) {
        //Billing Address section
        //static values
        helper.verifyMultipleValuesInOneElement(billingInfo, pageStaticValues.billingInfoLabels);

        //dynamic values
        helper.verifyText(nameBillingAddress, orderDetails.firstName + ' ' + orderDetails.lastName);
        helper.verifyText(emailBillingAddress, orderDetails.email);
        helper.verifyText(phoneBillingAddress, orderDetails.phone);
        helper.verifyText(address1BillingAddress, orderDetails.address);
        helper.verifyText(cityStateZipBillingAddress, orderDetails.city + ', ' + orderDetails.zip);
        helper.verifyText(countryBillingAddress, orderDetails.country);
        helper.verifyText(paymentMethod, `Payment Method: ` + paymentMethodValue);
        helper.verifyText(paymentMethodStatus, `Payment Status: ` + paymentStatusValue);
        return this;
    };

    this.verify_Shipping_Address_values = function (orderDetails, pageStaticValues, shippingMethodValue, shippingStatusValue) {
        //Shipping Address
        helper.verifyMultipleValuesInOneElement(shippingInfo, pageStaticValues.shippingInfoLabels);
        helper.verifyText(nameShippingAddress, orderDetails.firstName + ' ' + orderDetails.lastName);
        helper.verifyText(emailShippingAddress, orderDetails.email);
        helper.verifyText(phoneShippingAddress, orderDetails.phone);
        helper.verifyText(address1ShippingAddress, orderDetails.address);
        helper.verifyText(cityStateZipShippingAddress, orderDetails.city + ', ' + orderDetails.zip);
        helper.verifyText(countryShippingAddress, orderDetails.country);
        helper.verifyText(shippingMethod, `Shipping Method: ` + shippingMethodValue);
        helper.verifyText(shippingStatus, `Shipping Status: ` + shippingStatusValue);
        return this;
    };

    this.verify_Products_values = function (product) {

        helper.verifyText(subtotal, `Sub-Total: ` + product.total);
        helper.verifyText(shipping, `Shipping: ` + product.shipping);
        helper.verifyText(tax, `Tax: ` + product.tax);
        helper.verifyText(orderTotal, `Order Total: ` + product.total);

        helper.verifyText(skuProduct, product.SKU);
        helper.verifyText(productName, product.name);
        helper.verifyText(productName, product.size);
        helper.verifyText(productName, product.color);
        helper.verifyText(productUnitPrice, product.price);
        helper.verifyText(productSubTotal, product.total);
        helper.verifyText(productQuantity, product.quantity);
        return this;
    };

    this.verify_different_address = function(){
    expect(addressBillingFinal.getText()).not.toEqual(addressShippingFinal.getText());
    };

    this.verify_shipping_method = function(shippingMethod){
        expect(shippingMethodFinal.getText()).toEqual(shippingMethod);
    };

    this.verify_payment_method = function(paymentMethod){
        expect(paymentMethodFinal.getText()).toEqual(paymentMethod);
    };
  };
module.exports = new OrderDetailsPage();
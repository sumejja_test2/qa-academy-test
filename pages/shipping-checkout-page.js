const helper = require('../e2e-helper.js');
const shippingfirstNameInput = $('#ShippingNewAddress_FirstName');
const shippinglastNameInput = $('#ShippingNewAddress_LastName');
const shippingemailInput = $('#ShippingNewAddress_Email');
const shippingcityInput = $('#ShippingNewAddress_City');
const shippingaddress1Input = $('#ShippingNewAddress_Address1');
const shippingzipInput = $('#ShippingNewAddress_ZipPostalCode');
const shippingphoneInput = $('#ShippingNewAddress_PhoneNumber');
const shippingcountryDropdown = $('#ShippingNewAddress_CountryId');
const continueButton_ShippingAddress = $$('.new-address-next-step-button').get(1);
const continueButton_ShippingMethod = $('.shipping-method-next-step-button');
const continueButton_PaymentMethod = $('.payment-method-next-step-button');
const continueButton_PaymentInformation = $('.payment-info-next-step-button');
const confirmButton = $('.active').$('[value="Confirm"]');
const orderDetailsLink = element(by.linkText('Click here for order details.'));
const orderCompleted = $('.order-completed');
const shippingAddressContainer = $('#shipping-new-address-form');
const radioButtonNextDayAir= $(`[value="Next Day Air___Shipping.FixedByWeightByTotal"]`);
const radioButtonCreditCard= $(`[value="Payments.Manual"]`);

    const shippingCheckoutPage = function () {
        this.click_on_first_name_input= function() {
                helper.waitAndClick(shippingfirstNameInput);
                return this;
            };

        this.enter_all_required_values_shipping_address = function (orderDetailsShippingAddress) {
                helper.enterValue(shippingfirstNameInput, orderDetailsShippingAddress.firstNameShipping);
                helper.enterValue(shippinglastNameInput, orderDetailsShippingAddress.lastNameShipping);
                helper.enterValue(shippingemailInput, orderDetailsShippingAddress.emailShipping);
                helper.enterValue(shippingcityInput, orderDetailsShippingAddress.cityShipping);
                helper.enterValue(shippingaddress1Input, orderDetailsShippingAddress.addressShipping);
                helper.enterValue(shippingzipInput, orderDetailsShippingAddress.zipShipping);
                helper.enterValue(shippingphoneInput, orderDetailsShippingAddress.phoneShipping);

                    helper.selectDropdownOption(shippingcountryDropdown, orderDetailsShippingAddress.countryShipping);
                return this;
            };
        this.click_CONTINUE_on_Shipping_Address = function () {
                helper.click(continueButton_ShippingAddress);
                return this;
            };

                    this.click_CONTINUE_on_Shipping_Method = function () {
                helper.click(continueButton_ShippingMethod);
                return this;
            };
        this.click_radio_button_next_day_air= function(){
                helper.waitAndClick(radioButtonNextDayAir);
                return this;

                };
        this.click_radio_button_credit_card= function(){
                helper.waitAndClick(radioButtonCreditCard);
                return this;
            };


                this.click_CONTINUE_on_Payment_Method = function () {
                helper.click(continueButton_PaymentMethod);
                return this;
            };

            this.click_CONTINUE_on_Payment_Information = function () {
                helper.click(continueButton_PaymentInformation);
                return this;
            };

            this.click_CONTINUE_ORDER = function () {
                helper.click(confirmOrderButton);
                return this;
            };

            this.wait_Shipping_Address_to_load = function () {
                helper.waitVisibility(shippingAddressContainer);
                return this;
            };

            this.click_CONFIRM = function () {
                helper.click(confirmButton);
                return this;
            };

            this.click_ORDER_DETAILS_LINK = function () {
                helper.click(orderDetailsLink);
                return this;
            };

            this.verify_Order_completed_message = function (msg) {
                helper.verifyText(orderCompleted, msg);
                return this;
            };

            };

    module.exports = new shippingCheckoutPage();
const helper = require('../e2e-helper.js');
const addToCartButton = $('.add-to-cart-button');
//const shoesSizeDropdown = $('#product_attribute_input_9').$('#product_attribute_9');
const shoesSizeDropdown = $('#product_attribute_input_9').element(by.tagName('select'));
const shoesColor= $('[style="background-color:#363656"]');
const shoppingCartLinkOnNotification = $('#bar-notification').$('[href="/cart"]');


const ProductDetailsPage = function () {

    this.click_ADD_TO_CART_button = function () {
        helper.waitAndClick(addToCartButton)
        return this;
    };

    this.click_Shopping_cart_link_on_notification = function () {
        helper.waitAndClick(shoppingCartLinkOnNotification)
        return this;
    };

    this.select_shoes_size = function (value) {
        helper.selectDropdownOption(shoesSizeDropdown, value)
        return this;
    };

    this.select_shoes_color = function () {
        helper.waitPresenceAndClick(shoesColor);
        return this;
    };
};

module.exports = new ProductDetailsPage();

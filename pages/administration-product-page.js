const helper = require('../e2e-helper.js');
const L = require('../data-provider/localhost-data');
const catalog = $('.fa-book');
const products = $('[href="/Admin/Product/List"]');
const addNewProduct= $('[href="/Admin/Product/Create"]');
const productName= $(`#Name`);
const categories= $$(`.k-widget`).get(0);
const categoryDropdown= $(`#SelectedCategoryIds`);
const saveProductButton= $$(`.bg-blue`).get(0);
const publicStore= $(`[href="/"]`);
const adminLink= $(`[href="/Admin"]`);

const AdminProductPage = function () {

    this.click_administration_link = function () {
        helper.click(adminLink);
        return this;
    };

    this.select_catalog_item = function () {
        helper.click(catalog);
        helper.click(products);
        return this;
    };
    this.click_add_new_product = function () {
        helper.click(addNewProduct);
        return this;
    };
    this.enter_product_name = function (data) {
        helper.enterValue(productName,data);

        return this;
    };

    this.select_categories = function (data) {
        helper.selectDropdownOption(categories, data, categoryDropdown);
        return this;
    };

    this.click_save_button = function () {
        helper.click(saveProductButton);
        return this;
    };
    this.click_public_store = function () {
        helper.click(publicStore);
        return this;
    };


};

module.exports = new AdminProductPage();

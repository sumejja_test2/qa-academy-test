const helper = require('../e2e-helper.js');

const register = $('.ico-register');
const logIn = $('.ico-login');
const wishlist = $('.ico-wishlist');
const shoppingCart = $('.ico-cart');
const myAccount =$('.ico-account');

const Menu = function () {

    this.click_Register = function () {
    helper.click(register);
    return this;
    };

    this.click_Log_in =function () {
        helper.click(logIn);
        return this;
    };

    this.click_Wishlist = function () {
        helper.click(wishlist);
        return this;
    };

    this.click_Shopping_cart = function () {
      helper.click(shoppingCart);
      return this;
    };

    this.click_MyAccount = function () {
        helper.click(myAccount);
        return this;
};
};

module.exports = new Menu();
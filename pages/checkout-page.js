const helper = require('../e2e-helper.js');
const firstNameInput = $('#BillingNewAddress_FirstName');
const lastNameInput = $('#BillingNewAddress_LastName');
const emailInput = $('#BillingNewAddress_Email');
const cityInput = $('#BillingNewAddress_City');
const address1Input = $('#BillingNewAddress_Address1');
const zipInput = $('#BillingNewAddress_ZipPostalCode');
const phoneInput = $('#BillingNewAddress_PhoneNumber');
const countryDropdown = $('#BillingNewAddress_CountryId');
const shippingAddressContainer = $('#checkout-step-shipping');
const continueButton = $('.active').$('[value="Continue"]');
const checkboxShipToSameAddress = $('#ShipToSameAddress');
const continueButton_BillingAddress = $$('.new-address-next-step-button').get(0);
const newShippingAddressDropdown = $('#shipping-address-select').all(by.tagName('option')).get(1);
const continueButton_ShippingAddress = $$('.new-address-next-step-button').get(1);
const firstNameShipping = $('#ShippingNewAddress_FirstName');
const lastNameShipping = $('#ShippingNewAddress_LastName');
const emailShipping = $('#ShippingNewAddress_Email');
const cityShipping = $('#ShippingNewAddress_City');
const addressShipping = $('#ShippingNewAddress_Address1');
const zipShipping = $('#ShippingNewAddress_ZipPostalCode');
const phoneShipping = $('#ShippingNewAddress_PhoneNumber');
const countryShipping = $('#ShippingNewAddress_CountryId');
const nextDayAir = $('#shippingoption_1');
const continueButton_ShippingMethod = $('.shipping-method-next-step-button');
const creditCard = $('#paymentmethod_1');
const continueButton_PaymentMethod = $('.payment-method-next-step-button');
const creditCardTypeDropdown = $('#CreditCardType');
const cardHolderName = $('#CardholderName');
const cardNumber = $('#CardNumber');
const expireMonth = $('#ExpireMonth');
const expireYear = $('#ExpireYear');
const cardCode = $('#CardCode');
const continueButton_PaymentInformation = $('.payment-info-next-step-button');
const confirmOrderButton = $('.confirm-order-next-step-button');
const confirmButton = $('.active').$('[value="Confirm"]');
//const orderDetailsLink = element(by.linkText('Click here for order details.'));
const orderDetailsLink = $('.details-link a');
const orderCompleted = $('.order-completed');

const CheckoutPage = function () {

    this.select_new_shipping_address_dropdown= function () {
        helper.waitAndClick(newShippingAddressDropdown);
        return this;
    };

    this.click_checkbox_ship_to_same_address = function () {
        helper.waitAndClick(checkboxShipToSameAddress);
        return this;
    };

    this.enter_all_required_values_billing_address = function (orderDetails) {
        helper.enterValue(firstNameInput, orderDetails.firstName);
        helper.enterValue(lastNameInput, orderDetails.lastName);
        helper.enterValue(emailInput, orderDetails.email);
        helper.enterValue(cityInput, orderDetails.city);
        helper.enterValue(address1Input, orderDetails.address);
        helper.enterValue(zipInput, orderDetails.zip);
        helper.enterValue(phoneInput, orderDetails.phone);
        helper.selectDropdownOption(countryDropdown, orderDetails.country);
        return this;
    };

    this.click_checkbox_new_shipping_address = function () {
        helper.waitAndClick(newShippingAddressDropdown);
        return this;
    };

    this.enter_all_required_values_shipping_address = function (orderDetailsShippingAddress) {
        helper.enterValue(firstNameShipping, orderDetailsShippingAddress.firstNameShipping);
        helper.enterValue(lastNameShipping, orderDetailsShippingAddress.lastNameShipping);
        helper.enterValue(emailShipping, orderDetailsShippingAddress.emailShipping);
        helper.enterValue(cityShipping, orderDetailsShippingAddress.cityShipping);
        helper.enterValue(addressShipping, orderDetailsShippingAddress.addressShipping);
        helper.enterValue(zipShipping, orderDetailsShippingAddress.zipShipping);
        helper.enterValue(phoneShipping, orderDetailsShippingAddress.phoneShipping);
        helper.selectDropdownOption(countryShipping, orderDetailsShippingAddress.countryShipping);
        return this;
    };

    this.click_CONTINUE = function () {
        helper.click(continueButton);
        return this;
    };

    this.click_CONTINUE_on_Billing_Address = function () {
        helper.click(continueButton_BillingAddress);
        return this;
    };

    this.click_CONTINUE_on_Shipping_Address = function () {
        helper.click(continueButton_ShippingAddress);
        return this;
    };

    this.click_CONTINUE_on_Shipping_Method = function () {
        helper.click(continueButton_ShippingMethod);
        return this;
    };

    this.click_CONTINUE_on_Payment_Method = function () {
        helper.click(continueButton_PaymentMethod);
        return this;
    };

    this.click_CONTINUE_on_Payment_Information = function () {
        helper.click(continueButton_PaymentInformation);
        return this;
    };

    this.click_CONTINUE_ORDER = function () {
        helper.click(confirmOrderButton);
        return this;
    };

    this.wait_Shipping_Address_to_load = function () {
        helper.waitVisibility(shippingAddressContainer);
        return this;
    };

    this.click_CONFIRM = function () {
        helper.click(confirmButton);
        return this;
    };

    this.click_ORDER_DETAILS_LINK = function () {
        helper.click(orderDetailsLink);
        return this;
    };

    this.verify_Order_completed_message = function (msg) {
        helper.verifyText(orderCompleted, msg);
        return this;
    };

    this.choose_shipping_method = function () {
        helper.waitAndClick(nextDayAir);
        return this;
    };

    this.choose_payment_method = function(){
        helper.waitAndClick(creditCard);
        return this;
    };

    this.enter_payment_information = function (paymentInfo) {
        helper.selectDropdownOption(creditCardTypeDropdown, 'Visa');
        helper.enterValue(cardHolderName, paymentInfo.cardHolderName);
        helper.enterValue(cardNumber, paymentInfo.cardNumber);
        helper.enterValue(expireMonth, paymentInfo.expireMonth);
        helper.enterValue(expireYear, paymentInfo.expireYear);
        helper.enterValue(cardCode, paymentInfo.cardCode);
    };
};

module.exports = new CheckoutPage();

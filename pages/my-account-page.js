const helper = require('../e2e-helper.js');
const D = require('../data-provider/test-data');

const firstName = $('#FirstName');
const lastName = $('#LastName');
const email = $('#Email');
const changePassword = $('[href="/customer/changepassword"]');
const oldPassword = $('#OldPassword');
const newPassword = $('#NewPassword');
const saveButton = $('#save-info-button');
const confirmNewPassword = $('#ConfirmNewPassword');
const changePasswordButton = $('.button-1.change-password-button');
const messageChangedPassword = $('.page-body');

const MyAccountPage = function () {

    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;

    this.verify_all_values = function (data) {

        helper.verifyValue(firstName, data.firstNameReg);
        helper.verifyValue(lastName, data.lastNameReg);
        helper.verifyValue(email, data.emailReg);

        console.log('Verifying all Values are displayed');
        return this;
    };

    this.change_personal_info = function (data) {

        helper.enterValue(firstName, data.firstNameReg);
        helper.enterValue(lastName, data.lastNameReg);
        helper.enterValue(email, data.emailReg);
        helper.click(saveButton);

        console.log('Changing Personal Info on My Account Page');
        return this;
    };

    this.change_password_on_account_page = function () {
        helper.click(changePassword);
        helper.enterValue(oldPassword, D.registrationDetails.passwordReg);
        helper.enterValue(newPassword, 'newPassword')
        helper.enterValue(confirmNewPassword, 'newPassword');
        helper.click(changePasswordButton);

        console.log('Changing Password on My Account Page');
        return this;
    };

    this.verify_password_change_message = function (msg) {
        helper.verifyText(messageChangedPassword, msg);
        return this;
    };
};

module.exports = new MyAccountPage();
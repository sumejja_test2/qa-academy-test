const helper = require('../e2e-helper.js');
const C = require('../data-provider/constants.js');

const genderMale = $('#gender-male');
const genderFemale = $('#gender-female');
const firstName = $('#FirstName');
const lastName = $('#LastName');
const eMail = $('#Email');
const password = $('#Password');
const confirmPassword = $('#ConfirmPassword');
const registerButton = $('#register-button');
const result = $('.result');
const formContainer = $('[method="post"]');
const continueButton = $('.button-1.register-continue-button');
const cmessage = $('.result');

const RegistrationPage = function () {

    this.select_gender = function (gender) {
        const genderOption = helper.findElementByValue('label', gender);
        helper.click(genderOption);
        return this;
    };

    this.enter_all_required_values = function (data) {

        helper.enterValue(firstName, data.firstName);
        helper.enterValue(lastName, data.lastName);
        helper.enterValue(eMail, data.email);
        helper.enterValue(password, data.password);
        helper.enterValue(confirmPassword, data.confirmPassword);

        console.log('Registering customer with email ' + data.email);
        return this;
    };

    this.click_Register_button = function () {
        helper.click(registerButton);
        return this;
    };

    this.click_Continue_button = function () {
        helper.click(continueButton);
        return this;
    };

    this.verify_successful_registration = function () {
        helper.verifyText(result, C.pages.registration.successMessage);
        return this;
    };

    this.verify_validation_messages = function (msgs) {
        helper.verifyMultipleValuesInOneElement(formContainer, msgs);
        return this;
    };

    this.verify_validation_message = function (msg) {
        helper.verifyText(formContainer, msg);
        return this;
    };
};

module.exports = new RegistrationPage();
const helper = require('../e2e-helper.js');
const checkoutAsGuest = $('[value="Checkout as Guest"]');
const loginLink = $('.ico-login');
const emailInputField = $('.email')
const passwordInputField = $('.password');
const loginButton = $('.login-button');
const header = $('.header-upper');
const administratorLink = $('[href="/Admin"]');

const LoginPage = function () {

    this.click_CHECKOUT_AS_GUEST_button = function () {
        helper.click(checkoutAsGuest);
        return this;
    };

    this.click_Login_As_Admin = function (Data) {
        browser.get('http://localhost:15536');
        helper.click(loginLink);
        helper.enterValue(emailInputField, Data.emailAdmin);
        helper.enterValue(passwordInputField, Data.passwordAdmin);
        helper.click(loginButton);
        return this;
    };

    this.enter_Admin_Panel = function () {
        this.click_Login_As_Admin();
        helper.click(administratorLink);
        return this;
};
    };

module.exports = new LoginPage();

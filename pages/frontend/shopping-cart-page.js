const helper = require('../../e2e-helper.js');
const checkoutButton = $('[value="checkout"]');
const termsOfServiceCheckbox = $('.terms-of-service').$('[type="checkbox"]');

const ShoppingCartPage = function () {
    
    this.click_CHECKOUT_button = function () {
        helper.click(checkoutButton)
        return this;
    };

    this.click_terms_of_service_checkbox = function () {
        helper.click(termsOfServiceCheckbox);
        return this;
    };

};

module.exports = new ShoppingCartPage();

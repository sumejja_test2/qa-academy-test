const helper = require('../e2e-helper.js');
const C = require('./constants');
const D = exports;

D.randomNo = Math.floor(1000000 * Math.random() + 1).toString();

D.getNewRandomNo = function() {
    return Math.floor(1000000 * Math.random() + 1).toString();
};

D.accounts = {
    admin: {
        email: 'sumejja.test1@gmail.com',
        password: `Test123!`
    },
    customer: {
        email: 'sumejja.test2@gmail.com',
        password: `Test123!`
    }
};

D.registrationValues = {
    firstName: D.randomNo + '_auto-test_FirstName',
    lastName: D.randomNo + '_auto-test_LastName',
    email: D.randomNo + '_auto-test@yopmail.com',
    password: 'Test123',
    confirmPassword: 'Test123',
};

D.orderDetails = {
    firstName: D.randomNo + '_auto-test_FirstName',
    lastName: D.randomNo + '_auto-test_LastName',
    email: D.randomNo + '_auto-test@yopmail.com',
    city: D.randomNo + '_auto-test city',
    address: D.randomNo + '_auto-test address',
    zip: D.randomNo + '_auto-test zip',
    phone: D.randomNo + '_auto-test phone',
    country: 'Austria'
};

D.shippingDetails = {
    firstName: D.randomNo + '_Shipping_FirstName',
    lastName: D.randomNo + '_Shipping_LastName',
    email: D.randomNo + '_Shipping_auto-test@yopmail.com',
    city: D.randomNo + '_Shipping_auto-test city',
    address: D.randomNo + '_Shipping_auto-test address',
    zip: D.randomNo + '_Shipping_auto-test zip',
    phone: D.randomNo + '_Shipping_auto-test phone',
    country: 'Austria'
};

D.product = {
    name: 'adidas Consortium Campus 80s Running Shoes',
    size: '10',
    color: 'Blue',
    SKU: 'AD_C80_RS',
    price: '$27.56',
    total: '$27.56',
    quantity: 1,
    shipping: '$0.00',
    tax: '$0.00',
};

D.paymentInfo = {
    cardHolderName: D.randomNo + '_auto-test_CardHolderName',
    cardNumber: '4856608934969491',
    expireMonth: '12',
    expireYear: '2020',
    cardCode: '1234',
};

D.administratorAddProductData = {
    productName: helper.getRandomString(5) + 'New Product',
    shortDescription: 'This Product is one of a kind. Tra la la la la.',
    fullDescription: 'This Product is one of a kind. Tra la la la la.',
    SKU: helper.getRandomString(5) + '_SKU',
    category: C.productCategories.electronics.cellPhones,
    stockQuantity: 1000,
};



module.exports = D;

const helper = require('../e2e-helper.js');

const randomNo = Math.floor(10000 * Math.random() + 1).toString();

exports.accounts = {
    admin: {
        email: 'sumejja.test1@gmail.com',
        password: `Test123!`
    },
    customer: {
        email: 'sumejja.test2@gmail.com',
        password: `Test123!`
    }
};


exports.menuItem = {
    catalog: {
        products: 'Products',
        categories: 'Categories',
        manufacturers: 'Manufacturers',
        productReviews:'Product reviews',
        productTags:'Product tags',
        attributes: 'Attributes'
    }

};
exports.productValue = {
        productsName: helper.getRandomString(5),
        categories: {
            electronics: `Electronics`,
            electronicsCamera: `Electronics >> Camera & photo`,
            electronicsPhones: 'Electronics >> Cell phones',
            electronicsOthers: `Electronics >> Others`
        },
        manufacturers:'Manufacturers',
        productReviews: 'Product reviews',
        productTags: 'Product tags',
        attributes: 'Attributes'

};

module.exports = exports;
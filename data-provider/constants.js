const helper = require('../e2e-helper.js');
const randomNo = Math.floor(1000000000 * Math.random() +1).toString();

exports.environments = {
    local: 'http://localhost:15536/',
    staging: 'https://demo.nopcommerce.com'
};

exports.baseUrl = exports.environments.staging;


exports.genderOption = {
    male: 'Male',
    female: 'Female',
};

exports.paymentMethod = {
    check_moneyOrder: 'Check / Money Order',
    creditCard: 'Credit Card',
};

exports.paymentStatus = {
    pending: 'Pending'
};

exports.shippingMethod = {
    ground: 'Ground',
    nextDayAir: 'Next Day Air',
    secondDayAir: '2nd Day Air',
};

exports.shippingStatus = {
    notYetShipped: 'Not yet shipped'
};

exports.pages = {
    registration: {
        successMessage: 'Your registration completed',
        validationMessages: {
            requiredFields: [
                'First name is required.',
                'Last name is required.',
                'Email is required.',
                'Password is required.'
            ],
            existingEmail: 'The specified email already exists',
            wrongEmail: 'Wrong email',
            passwordTooShort: 'Password must meet the following rules:\nmust have at least 6 characters',
            passwordsDoNotMatch: 'The password and confirmation password do not match.'
        }
    },
    orderDetails: {
        title: 'Order Information',
        billingInfoLabels: ['Billing Address', 'Payment'],
        shippingInfoLabels: ['Shipping Address', 'Shipping'],
    },
    shippingDetails: {
        title: 'Order Information',
        billingInfoLabels: ['Billing Address', 'Payment'],
        shippingInfoLabels: ['Shipping Address', 'Shipping'],
    },
    myAccountDetails: {
        messageChangedPassword: 'Password was changed',
    },
    administration: {
        addProduct: {
            successMessage: 'The new product has been added successfully.'
        }
    }
};

exports.productCategories = {
    computers:
        {
            main: 'Computers',
            desktops: 'Computers >> Desktops'
        },
    electronics:
        {
            main: 'Electronics',
            cellPhones: 'Electronics >> Cell phones'
        },
};

exports.taxCategoryDropdown = {
    value1: 'Books',
    value2: 'Electronics & Software',
    value3: 'Downloadable Products',
    value4: 'Jewelry',
    value5: 'Apparel'
};

exports.inventoryMethodDropdown = {
    value0: 'Dont track inventory',
    value1: 'Track inventory',
    value2: 'Track inventory by product attributes'
};

module.exports = exports;
exports.config = {
    capabilities:
        {browserName: 'chrome',
        },

    specs: [
       //'./tests/checkout-test.js'
       //'./tests/checkout-test-drugi-dio.js'
       './tests/registration-test.js',
        // './tests/my-account-test.js'
        './tests/add_new_product_test.js'
    ],

    directConnect: true,
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 120000
    },
    onPrepare: function() {
        browser.ignoreSynchronization = true;
        browser.driver.manage().window().maximize();

    },
    plugins: [{
        package: 'protractor-screenshoter-plugin',
        screenshotPath: './report-with-screenshots',
        screenshotOnExpect: 'failure+success',
        screenshotOnSpec: 'failure+success',
        withLogs: 'false',
        writeReportFreq: 'asap',
        imageToAscii: 'none',
        clearFoldersBeforeTest: true
    }],
};
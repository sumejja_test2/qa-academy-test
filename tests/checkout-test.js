const productDetails = require('../pages/product-details-page.js');
const shoppingCart = require('../pages/frontend/shopping-cart-page');
const page = require('../pages/checkout-page');
const login = require('../pages/login-page');
const orderDetails = require('../pages/order-details-page');
const D = require('../data-provider/test-data');
const helper = require('../e2e-helper');

fdescribe('Testing Email page', function () {

    beforeAll(function () {
        const currentDate = helper.getCurrentDateAndTime('dddd, mmmm dd, yyyy');
        console.log('Current date is ............... ' + currentDate);
    });

    beforeEach(function () {
        browser.driver.manage().deleteAllCookies();
        browser.get('https://demo.nopcommerce.com/adidas-consortium-campus-80s-running-shoes');
    });

    xit(' Verify validation message for blank start date', function () {

        productDetails.select_shoes_size('10')
            .click_ADD_TO_CART_button()
            .click_Shopping_cart_link_on_notification();
        shoppingCart.click_terms_of_service_checkbox()
            .click_CHECKOUT_button();
        login.click_CHECKOUT_AS_GUEST_button();
        page.enter_all_required_values_billing_address(D.orderDetails)
            .click_CONTINUE_on_Billing_Address()
            .click_CONTINUE_on_Shipping_Method()
            .click_CONTINUE_on_Payment_Method()
            .click_CONTINUE_on_Payment_Information()
            .click_CONFIRM()
            .verify_Order_completed_message('Your order has been successfully processed!');

        browser.sleep(5000)

    });

    it(' Verify that the order was successfully completed, then check Order Details page and its data', function () {

        productDetails.select_shoes_size('10')
            .click_ADD_TO_CART_button()
            .click_Shopping_cart_link_on_notification();
        shoppingCart.click_terms_of_service_checkbox()
            .click_CHECKOUT_button();
        login.click_CHECKOUT_AS_GUEST_button();
        page.enter_all_required_values_billing_address(D.orderDetails)
            .click_CONTINUE_on_Billing_Address()
            .click_CONTINUE_on_Shipping_Method()
            .click_CONTINUE_on_Payment_Method()
            .click_CONTINUE_on_Payment_Information()
            .click_CONFIRM()
            .verify_Order_completed_message('Your order has been successfully processed!')
            .click_ORDER_DETAILS_LINK();
        orderDetails.verify_Order_date(`Order Date: ` + helper.currentDate)
            .verify_Billing_Address_values(
                D.orderDetails,
                D.pages.orderDetails,
                D.paymentMethod.check_moneyOrder,
                D.paymentStatus.pending)
            .verify_Shipping_Address_values(
                D.orderDetails,
                D.pages.orderDetails,
                D.shippingMethod.ground,
                D.shippingStatus.notYetShipped
            )
            .verify_Products_values(D.product)


    });
});
